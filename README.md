# Noitcereon CV (Work Profile Website)
[![Netlify Status](https://api.netlify.com/api/v1/badges/a5864483-63a4-428d-a8d3-e5fbada5e43f/deploy-status)](https://app.netlify.com/sites/whoisthomasandersen/deploys)

This repository contains a website, that represents showcases my work profile in a way that is relevant to web development.

It could potentially be forked and modified to match your profile.

## Design in Figma

I made the design in Figma, which can be seen here: https://www.figma.com/file/zNq5kwqTSY5stzTN2efveO/Thomas-CV?node-id=0%3A1

Inspiration to my design: http://findmatthew.com/

## Installation

Fairly standard JS framework installation.

1. Clone or fork this repository
2. Navigate to the folder you cloned/forked to
3. Run the `npm install` command to install dependencies
4. Run `npm run dev` to run a development localhost server on port 3000

## Usage

Usage can be split into two categories: seeing this website in action and modifying it for personal use.

### Live Website

https://whoisthomasandersen.com

### For personal use
To properly use this project you need HTML, CSS (with TailwindCSS) and JavaScript (with Vue) knowledge. Especially if you want to modify it aside from the text.

As this is primarily meant for my personal use, the contents of the site need to be heavily modified in regards to text + a few images that need to be replaced.

The project is split up into Vue components, so finding what you're looking to change should be fairly easy.

Example: if you want to change the contact information you go the Contact.vue file, which is located at ./src/components/Contact.vue

## Maintainer

@Noitcereon (Thomas Andersen)


