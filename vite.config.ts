/// <reference types="vitest" />
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/noitcereon-cv/", // Should be "/" when deploying to Netlify, but "/noitcereon-cv/" when deploying to Gitlab Pages. (handled via a different build command)
  plugins: [vue()],

  test: {},
});
