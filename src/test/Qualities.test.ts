import { afterEach, beforeEach, describe, expect, test } from "vitest";
import { mount, VueWrapper, enableAutoUnmount, shallowMount } from "@vue/test-utils";
import Qualities from "../components/Qualities.vue";

// Given_Preconditions_When_StateUnderTest_Then_ExpectedBehavior (test naming)
// Example: Given_UserIsAuthenticated_When_InvalidAccountNumberIsUsedToWithdrawMoney_Then_TransactionsWillFail

describe("Qualities", () => {
  let wrapper: VueWrapper<any>;
  enableAutoUnmount(afterEach);

  beforeEach(() => {
    wrapper = shallowMount(Qualities);
    expect(Qualities).toBeTruthy();
  });
  

  test("Given_QualitiesComponent_When_ExpectedTextIsNotPresent_Then_TestWillFail", async () => {
    const actualQualitiesText = wrapper.text();
    const expectedQuality1 = "Flexible";
    const expectedQuality2 = "Team Player";
    const expectedQuality3 = "Learner";
    expect(actualQualitiesText).toContain(expectedQuality1);
    expect(actualQualitiesText).toContain(expectedQuality2);
    expect(actualQualitiesText).toContain(expectedQuality3);
  });
});
