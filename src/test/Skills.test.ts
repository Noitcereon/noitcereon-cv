import { afterEach, beforeEach, describe, expect, test } from "vitest";
import { VueWrapper, enableAutoUnmount, shallowMount } from "@vue/test-utils";
import Skills from "../components/Skills.vue";

// Given_Preconditions_When_StateUnderTest_Then_ExpectedBehavior (test naming)
// Example: Given_UserIsAuthenticated_When_InvalidAccountNumberIsUsedToWithdrawMoney_Then_TransactionsWillFail

describe("Skills", () => {
  let wrapper: VueWrapper<any>;
  enableAutoUnmount(afterEach);

  beforeEach(() => {
    wrapper = shallowMount(Skills);
    expect(Skills).toBeTruthy();
  });
  

  test("Given_SkillsComponent_When_ExpectedTextIsNotPresent_Then_TestWillFail", async () => {
    const actualSkillsText = wrapper.text();
    const expectedInHeadline = "Skills";
    const expectedSkill1 = "C#";
    const expectedSkill2 = "HTML";
    const expectedSkill3 = "Docker";
    // There should be more skills, but just testing if there is more than one.
    
    expect(actualSkillsText).toContain(expectedInHeadline);
    expect(actualSkillsText).toContain(expectedSkill1);
    expect(actualSkillsText).toContain(expectedSkill2);
    expect(actualSkillsText).toContain(expectedSkill3);
  });
});
