import { afterEach, beforeEach, describe, expect, test } from "vitest";
import { mount, VueWrapper, enableAutoUnmount, shallowMount } from "@vue/test-utils";
import Contact from "../components/Contact.vue";

// Given_Preconditions_When_StateUnderTest_Then_ExpectedBehavior (test naming)
// Example: Given_UserIsAuthenticated_When_InvalidAccountNumberIsUsedToWithdrawMoney_Then_TransactionsWillFail

describe("Contact", () => {
  let wrapper: VueWrapper<any>;
  enableAutoUnmount(afterEach);

  beforeEach(() => {
    wrapper = shallowMount(Contact);
    expect(Contact).toBeTruthy();
  });
  

  test("Given_ContactComponent_When_ExpectedTextIsNotPresent_Then_TestWillFail", async () => {
    const actualContactText = wrapper.text();
    const expectedContactEmailContent = "tba@live.dk";
    const expectedWordInContactText = "Email";
    expect(actualContactText).toContain(expectedContactEmailContent);
    expect(actualContactText).toContain(expectedWordInContactText);
  });
});
