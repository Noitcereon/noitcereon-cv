import { afterEach, beforeEach, describe, expect, test } from "vitest";
import { mount, VueWrapper, enableAutoUnmount, shallowMount } from "@vue/test-utils";
import Footer from "../components/Footer.vue";

// Given_Preconditions_When_StateUnderTest_Then_ExpectedBehavior (test naming)
// Example: Given_UserIsAuthenticated_When_InvalidAccountNumberIsUsedToWithdrawMoney_Then_TransactionsWillFail

describe("Footer", () => {
  let wrapper: VueWrapper<any>;
  enableAutoUnmount(afterEach);

  beforeEach(() => {
    wrapper = shallowMount(Footer);
    expect(Footer).toBeTruthy();
  });
  

  test("Given_FooterComponent_When_ExpectedTextIsNotPresent_Then_TestWillFail", async () => {
    const actualFooterText = wrapper.text();
    const expectedTextInFooter = "© 2022-";
    const expectedTextInFooter2 = "Lineicons";
    expect(actualFooterText).toContain(expectedTextInFooter);
    expect(actualFooterText).toContain(expectedTextInFooter2);
  });
});
