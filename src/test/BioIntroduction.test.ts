import { afterEach, beforeEach, describe, test, expect } from "vitest";
import { enableAutoUnmount, mount, shallowMount, VueWrapper } from "@vue/test-utils";
import BioIntroduction from "../components/BioIntroduction.vue";

describe("BioIntroduction", () => {
  let wrapper: VueWrapper<any>;
  beforeEach(() => {
    wrapper = shallowMount(BioIntroduction);
    expect(BioIntroduction).toBeTruthy;
  });
  enableAutoUnmount(afterEach);

  test("Given_BioIntroduction_When_ExpectedTextIsNotPresent_Then_TestWillFail", async () => {
    const expectedHeadline = "The Person in Question";
    expect(wrapper.html()).toContain(expectedHeadline);
  });
});
