import { afterEach, beforeEach, describe, expect, test } from "vitest";
import { mount, VueWrapper, enableAutoUnmount, shallowMount } from "@vue/test-utils";
import About from "../components/About.vue";

// Given_Preconditions_When_StateUnderTest_Then_ExpectedBehavior (test naming)
// Example: Given_UserIsAuthenticated_When_InvalidAccountNumberIsUsedToWithdrawMoney_Then_TransactionsWillFail

describe("About", () => {
  let wrapper: VueWrapper<any>;
  enableAutoUnmount(afterEach);

  beforeEach(() => {
    wrapper = shallowMount(About);
    expect(About).toBeTruthy();
  });

  test("Given_AboutComponent_When_ExpectedTextIsNotPresent_Then_TestWillFail", async () => {
    const actualHomeText = wrapper.text();
    const expectedInHeadline = "About";
    expect(actualHomeText).toContain(expectedInHeadline);
  });
});
