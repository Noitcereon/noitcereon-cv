import { afterEach, beforeEach, describe, expect, test } from "vitest";
import { mount, VueWrapper, enableAutoUnmount, shallowMount } from "@vue/test-utils";
import Home from "../components/Home.vue";

// Given_Preconditions_When_StateUnderTest_Then_ExpectedBehavior (test naming)
// Example: Given_UserIsAuthenticated_When_InvalidAccountNumberIsUsedToWithdrawMoney_Then_TransactionsWillFail

describe("Home", () => {
  let wrapper: VueWrapper<any>;
  enableAutoUnmount(afterEach);

  beforeEach(() => {
    wrapper = shallowMount(Home);
    expect(Home).toBeTruthy();
  });
  

  test("Given_HomeComponent_When_ExpectedTextIsNotPresent_Then_TestWillFail", async () => {
    const actualHomeText = wrapper.text();
    const expectedInHeadline = "Thomas Andersen";
    const expectedButtonText = "Go to my skills";
    expect(actualHomeText).toContain(expectedInHeadline);
    expect(actualHomeText).toContain(expectedButtonText);
  });
});
